<?php
header('Content-Type: text/plain; charset=utf-8'); // Ž
require_once 'crwl.php';

class H4C {
    const URL = 'http://update.hicloud.com:8180/TDS/data/files/p3/s15/G753/g104';

    function url_cl($v) {
        return self::URL.'/v'.$v.'/f1/full/changelog.xml';
    }

    function prs($v, $chk = false) {
        $url = $this->url_cl($v);

        $this->dl->setUrl($url);
        $r = $this->dl->get(true);

        if ($r[1]['http_code'] === 404) {
            //echo '-', $url, "\n";
            return -1;
        }

        $dom = new DOMDocument();
        $dom->loadXML($r[0]);
        $ver = $dom->getElementsByTagName('component');
        if (!$ver->length) {
            echo 'Error: ', $v, "\n";
            echo $r[0], "\n";
            var_dump($e);
            die();
        }

        $ver = $ver->item(0)->getAttribute('version');

        if (strpos($ver, 'CHM-U01') === false) {
            return -2;
        }

        if ($chk) {
            echo $v, ', ';
            return;
        }

        $lng = $dom->getElementsByTagName('default-language')->item(0)
            ->getAttribute('name');

        $x = $dom->getElementsByTagName('language');
        $d = null;
        foreach ($x as $b) {
            if ($b->getAttribute('name') !== $lng) continue;
            $d = $b;
            break;
        }

        echo $ver, "\n";
        echo $dom->saveXML($d), "\n\n";
        //echo $r[0];
    }

    function __construct() {
        $this->dl = new Sugar\Crwl();
    }

    function go() {
        $db = [ 56029, 56219, 56223, 56789, 56798, 57384, 57392 ];
        foreach ($db as $v) $this->prs($v);
    }

    function chk_upd() {
        $v = 0;
        for ($i = 0; $i <= 100; $i++) {
            $v = 60840 + $i;
            $this->prs($v, true);
        }
        echo "\n", 'Last checked: ', $v, "\n";
    }
}

$x = new H4C();
//$x->chk_upd();
$x->go();
?>
